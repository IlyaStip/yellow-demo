export interface IJog {
    id: number;
    distance: number;
    time: number;
    date: Date;
    speed: number;
}
