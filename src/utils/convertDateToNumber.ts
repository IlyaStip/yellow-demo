const convertDateToNumber = (date: Date): number => new Date(date).getTime();

export default convertDateToNumber;
