const convertDateToFormat = (date: Date): string => {
    const convertibleВate = new Date(date);
    const month = convertibleВate.getMonth() + 1;
    const day = convertibleВate.getDate();
    const year = convertibleВate.getFullYear();

    return `${day < 10 ? `0${day}` : day}.${month < 10 ? `0${month}` : month}.${year}`;
};

export default convertDateToFormat;
