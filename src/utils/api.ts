import axios from "axios";

export default axios.create({
    baseURL: "https://jogtracker.herokuapp.com/api",
    responseType: "json",
    headers: {
        common: {
            "Content-Type": "application/x-www-form-urlencoded",
            Accept: "application/json"
        }
    }
});
