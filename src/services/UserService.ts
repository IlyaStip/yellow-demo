import api from "../utils/api";

const getUserAccessToken = async (): Promise<{ token: string; expires: number }> => {
    try {
        const respons = await api.post("/v1/auth/uuidLogin", { uuid: "hello" });

        return {
            token: `${respons.data.response.token_type} ${respons.data.response.access_token}`,
            expires: (respons.data.response.created_at + respons.data.response.expires_in) * 1000
        };
    } catch (e) {
        console.log(e);

        return {
            token: "",
            expires: Date.now()
        };
    }
};

export default getUserAccessToken;
