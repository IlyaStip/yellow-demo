import Cookies from "js-cookie";

import api from "../utils/api";
import convertDateToNumber from "../utils/convertDateToNumber";
import { IJog } from "../models/IJog";

export const getAllJogs = async (): Promise<IJog[]> => {
    try {
        const respons = await api.get("/v1/data/sync", { headers: { Authorization: Cookies.get("token") } });
        const { jogs } = respons.data.response;

        return jogs.map((jog: IJog) => {
            const date: number = convertDateToNumber(jog.date);

            return {
                id: jog.id,
                distance: jog.distance,
                time: jog.time,
                date: date * 1000,
                speed: 15
            };
        });
    } catch (e) {
        console.log(e);

        return [];
    }
};

export const createJog = async (date: Date, time: number, distance: number): Promise<void> => {
    try {
        await api.post(
            "/v1/data/jog",
            {
                date,
                time,
                distance
            },
            {
                headers: { Authorization: Cookies.get("token") }
            }
        );
    } catch (e) {
        console.log(e);
    }
};
