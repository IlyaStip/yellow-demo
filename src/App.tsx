import React, { FunctionComponent } from "react";
import { BrowserRouter } from "react-router-dom";

import RootRouter from "./router";
import { SGlobal } from "./styles/style";

const App: FunctionComponent = () => (
    <BrowserRouter>
        <SGlobal />
        <RootRouter />
    </BrowserRouter>
);

export default App;
