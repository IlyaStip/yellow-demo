export const white = "#ffffff";
export const gray = "#eaeaea";
export const darkGray = "#6c6c6c";
export const christi = "#62aa14";
export const green = "#7ed321";
export const blue = "#e990f9";
export const black = "#4a4a4a";
