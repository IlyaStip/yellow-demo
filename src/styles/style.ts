import styled, { createGlobalStyle } from "styled-components";

import "react-datepicker/dist/react-datepicker.css";
import { headerHeightDesktop } from "../components/Header/HeaderDesktop/style";
import { headerHeightMobile } from "../components/Header/HeaderMobile/style";

export const SGlobal = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
`;

export const SPage = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;

    width: 100vw;
    height: 100vh;

    padding-top: ${headerHeightDesktop}px;

    @media (max-width: 768px) {
        padding-top: ${headerHeightMobile}px;
        align-items: flex-start;
    }
`;
