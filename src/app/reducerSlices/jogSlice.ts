import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { IJog } from "../../models/IJog";
import { createJog, getAllJogs } from "../../services/JogService";
import { AppThunk, RootState } from "../store";

interface IJogState {
    jogs: IJog[];
    dateFrom: Date | null;
    dateTo: Date | null;
}

const initialState: IJogState = {
    jogs: [],
    dateFrom: null,
    dateTo: null
};

export const jogSlice = createSlice({
    name: "jogs",
    initialState,
    reducers: {
        setJogs: (state, action: PayloadAction<IJog[]>) => {
            state.jogs = action.payload;
        },
        setDateFrom: (state, action: PayloadAction<Date | null>) => {
            state.dateFrom = action.payload;
        },
        setDateTo: (state, action: PayloadAction<Date | null>) => {
            state.dateTo = action.payload;
        }
    }
});

export const { setJogs, setDateFrom, setDateTo } = jogSlice.actions;

export const getJogs = (state: RootState): IJog[] => state.jogs.jogs;
export const getDateFrom = (state: RootState): Date | null => state.jogs.dateFrom;
export const getDateTo = (state: RootState): Date | null => state.jogs.dateTo;

export default jogSlice.reducer;

export const loadJogs = (): AppThunk => async (dispatch): Promise<void> => {
    try {
        const allJogs = await getAllJogs();

        dispatch(setJogs(allJogs.reverse()));
    } catch (e) {
        console.log(e);
    }
};

export const addNewJog = (date: Date, time: number, distance: number): AppThunk => async (): Promise<void> => {
    try {
        await createJog(date, time, distance);
    } catch (e) {
        console.log(e);
    }
};
