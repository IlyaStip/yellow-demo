import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import Cookies from "js-cookie";

import { AppThunk, RootState } from "../store";
import getUserAccessToken from "../../services/UserService";

interface IUserState {
    accessToken: string;
    isAuth: boolean;
}

const initialState: IUserState = {
    accessToken: "",
    isAuth: false
};

export const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
        setAccessToken: (state, action: PayloadAction<string>) => {
            state.accessToken = action.payload;
            state.isAuth = true;
        },
        setIsAuth: (state, action: PayloadAction<boolean>) => {
            state.isAuth = action.payload;
        }
    }
});

export const { setAccessToken, setIsAuth } = userSlice.actions;

export const getAccessToken = (state: RootState): string => state.user.accessToken;
export const getIsAuth = (state: RootState): boolean => state.user.isAuth;

export default userSlice.reducer;

export const checkIsAuthorized = (): AppThunk => (dispatch): boolean => {
    const toketFromCookie = Cookies.get("token");

    if (toketFromCookie) {
        dispatch(setAccessToken(toketFromCookie));

        return true;
    }
    dispatch(setIsAuth(false));

    return false;
};

export const createAccessToken = (): AppThunk => async (dispatch): Promise<void> => {
    try {
        const { token, expires } = await getUserAccessToken();

        Cookies.set("token", token, {
            expires
        });
        dispatch(setAccessToken(token));
    } catch (e) {
        console.log(e);
    }
};
