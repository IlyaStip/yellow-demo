import { configureStore, combineReducers, Action } from "@reduxjs/toolkit";
import { ThunkAction } from "redux-thunk";

import jogReducer from "./reducerSlices/jogSlice";
import userReducer from "./reducerSlices/userSlice";

const rootReducer = combineReducers({
    jogs: jogReducer,
    user: userReducer
});

export const store = configureStore({
    reducer: rootReducer
});

export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>;
