import React, { FunctionComponent } from "react";
import { useHistory } from "react-router";

import Button, { ButtonTypes } from "../Button";

import { SNothingToShowWrapper, SNothingToShow, SNothingToShowText, SSadFace } from "./style";

const NothingToShow: FunctionComponent = () => {
    const history = useHistory();

    return (
        <SNothingToShowWrapper>
            <SNothingToShow>
                <SSadFace />
                <SNothingToShowText>Nothing is there</SNothingToShowText>
                <Button text="Create your jog first" onClick={() => history.push("/add")} styleType={ButtonTypes.Blue} />
            </SNothingToShow>
        </SNothingToShowWrapper>
    );
};

export default NothingToShow;
