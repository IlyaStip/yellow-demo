import styled from "styled-components";

import { ReactComponent as SadFace } from "../../static/sad-face.svg";

export const SNothingToShowWrapper = styled.div`
    width: 100%;
    height: 100%;

    display: flex;
    align-content: center;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
`;

export const SNothingToShow = styled.div`
    flex: 1 1 100%;

    display: flex;
    align-content: center;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
`;

export const SNothingToShowText = styled.p`
    flex: 1 1 100%;
    font-family: "Roboto", sans-serif;
    font-size: 24px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.21;
    letter-spacing: normal;
    text-align: center;
    margin-bottom: 140px;
    margin-top: 45px;
`;

export const SSadFace = styled(SadFace)`
    @media (max-width: 640px) {
        width: 85px;
        height: auto;
    }
`;
