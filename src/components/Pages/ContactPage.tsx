import React, { FunctionComponent } from "react";

import { SPage } from "../../styles/style";
import Header from "../Header";
import InfoBlock from "../InfoBlock";

export const ContactPage: FunctionComponent = () => (
    <>
        <Header showMenu showFilterBtn={false} />
        <SPage>
            <InfoBlock />
        </SPage>
    </>
);

export default ContactPage;
