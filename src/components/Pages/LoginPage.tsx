import React, { FunctionComponent } from "react";

import { SPage } from "../../styles/style";
import Header from "../Header";
import LoginForm from "../LoginForm";

export const LoginPage: FunctionComponent = () => (
    <>
        <Header showMenu={false} showFilterBtn={false} />
        <SPage>
            <LoginForm />
        </SPage>
    </>
);

export default LoginPage;
