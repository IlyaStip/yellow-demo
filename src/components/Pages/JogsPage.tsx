import React, { FunctionComponent } from "react";

import { SPage } from "../../styles/style";
import Header from "../Header";
import DataFilter from "../DataFilter";
import AddJogButton from "../AddJogButton";
import JogsList from "../JogsList";

export const JogsPage: FunctionComponent = () => (
    <>
        <Header showMenu showFilterBtn>
            <DataFilter />
        </Header>
        <SPage>
            <JogsList />
            <AddJogButton />
        </SPage>
    </>
);

export default JogsPage;
