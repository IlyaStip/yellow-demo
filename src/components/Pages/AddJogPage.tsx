import React, { FunctionComponent } from "react";

import { SPage } from "../../styles/style";
import AddJogFrom from "../AddJogForm";
import Header from "../Header";

export const JogsPage: FunctionComponent = () => {
    return (
        <>
            <Header showMenu showFilterBtn={false} />
            <SPage>
                <AddJogFrom />
            </SPage>
        </>
    );
};

export default JogsPage;
