import { Link } from "react-router-dom";
import styled from "styled-components";

import { white } from "../../styles/colors";

const SAddJogLink = styled(Link)`
    position: fixed;
    right: 40px;
    bottom: 40px;

    width: 60px;
    height: 60px;

    border-radius: 50%;
    background-color: ${white};
`;

export default SAddJogLink;
