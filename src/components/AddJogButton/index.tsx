import React, { FunctionComponent } from "react";

import { ReactComponent as Add } from "../../static/add.svg";

import SAddJogLink from "./style";

export const AddJogButton: FunctionComponent = () => {
    return (
        <SAddJogLink to="/add">
            <Add />
        </SAddJogLink>
    );
};

export default AddJogButton;
