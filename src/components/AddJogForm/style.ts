import { Link } from "react-router-dom";
import styled from "styled-components";

import { darkGray, green, white } from "../../styles/colors";
import { SDatePicker } from "../DataFilter/style";

export const SJogFormWrapper = styled.div`
    width: 100%;
    height: 100%;

    display: flex;
    justify-content: center;
    align-items: center;
`;

export const SJogForm = styled.form`
    position: relative;
    max-width: 505px;
    max-height: 380px;

    flex: 1 1 100%;

    display: flex;
    flex-wrap: wrap;

    padding: 100px 90px 45px;
    border-radius: 44px;
    background-color: ${green};

    .react-datepicker-wrapper {
        width: 100%;
    }

    @media (max-width: 768px) {
        margin: 34px;
        padding: 64px 35px 37px;
    }
`;

export const SFormText = styled.p`
    flex: 1 0 25%;
    font-family: "Roboto", sans-serif;
    font-size: 16px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
`;

export const SFormLabel = styled.label`
    flex: 1 1 100%;
    display: flex;
    justify-content: space-between;
    align-content: center;
    flex-wrap: nowrap;
    margin-bottom: 25px;

    @media (max-width: 768px) {
        flex-wrap: wrap;
        margin-bottom: 20px;
    }
`;

export const SFormInput = styled.input`
    flex: 1 0 75%;
    height: 31px;
    margin: 0 45px 0 15px;
    border-radius: 11px;
    border: solid 1px ${darkGray};
    padding: 10px 20px;
    background-color: ${white};
    margin: 0;
`;

export const SFormDataPicker = styled(SDatePicker)`
    width: 100%;
    margin: 0px;
`;

export const SFormClose = styled(Link)`
    position: absolute;
    top: 25px;
    right: 25px;

    @media (max-width: 768px) {
        top: 20px;
        right: 20px;

        svg {
            width: 20px;
            height: auto;
        }
    }
`;
