import React, { ChangeEvent, FunctionComponent, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";

import Button, { ButtonTypes } from "../Button";
import { dataFormats } from "../DataFilter";
import { ReactComponent as Close } from "../../static/cancel.svg";
import { addNewJog } from "../../app/reducerSlices/jogSlice";

import { SJogForm, SFormText, SFormLabel, SFormInput, SFormDataPicker, SFormClose, SJogFormWrapper } from "./style";

export const JogsPage: FunctionComponent = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const [date, setDate] = useState<Date | null>();
    const [distance, setDistance] = useState<number | undefined>();
    const [time, setTime] = useState<number | undefined>();

    const handleDistance = (e: ChangeEvent<HTMLInputElement>) => setDistance(Number(e.target.value));
    const handleTime = (e: ChangeEvent<HTMLInputElement>) => setTime(Number(e.target.value));

    return (
        <SJogFormWrapper>
            <SJogForm
                onSubmit={e => {
                    e.preventDefault();
                    if (!date || !distance || !time) return;
                    dispatch(addNewJog(date, time, distance));
                    history.push("/jogs");
                }}
            >
                <SFormClose to="/jogs">
                    <Close />
                </SFormClose>
                <SFormLabel>
                    <SFormText>Distance</SFormText>
                    <SFormInput type="number" value={distance} onChange={handleDistance} />
                </SFormLabel>
                <SFormLabel>
                    <SFormText>Time</SFormText>
                    <SFormInput type="number" value={time} onChange={handleTime} />
                </SFormLabel>
                <SFormLabel>
                    <SFormText>Date</SFormText>
                    <SFormDataPicker
                        dateFormat={dataFormats}
                        selectsStart
                        selected={date}
                        onChange={newDate => {
                            if (Array.isArray(newDate)) return;
                            setDate(newDate);
                        }}
                    />
                </SFormLabel>
                <Button text="Save" styleType={ButtonTypes.White} buttonType="submit" fillSpace />
            </SJogForm>
        </SJogFormWrapper>
    );
};

export default JogsPage;
