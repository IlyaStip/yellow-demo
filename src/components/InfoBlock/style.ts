import styled from "styled-components";

import { darkGray, green } from "../../styles/colors";

export const SInfoBlock = styled.div`
    @media (max-width: 768px) {
        margin: 25px;
    }
`;

export const SText = styled.p`
    max-width: 614px;
    font-family: "Roboto", sans-serif;
    font-size: 14px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.71;
    letter-spacing: normal;
    color: ${darkGray};
    margin-bottom: 45px;

    @media (max-width: 768px) {
        font-size: 12px;
    }
`;

export const STitle = styled.h2`
    font-family: "Roboto", sans-serif;
    font-size: 36px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: left;
    color: ${green};
    text-transform: uppercase;

    margin-bottom: 15px;

    @media (max-width: 768px) {
        font-size: 24px;
    }
`;
