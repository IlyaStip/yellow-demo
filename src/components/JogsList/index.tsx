import React, { FunctionComponent, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getDateFrom, getDateTo, getJogs, loadJogs } from "../../app/reducerSlices/jogSlice";
import { IJog } from "../../models/IJog";
import NothingToShow from "../NothingToShow";
import Jog from "../Jog";

import SJogWrapper from "./style";

export const JogsList: FunctionComponent = () => {
    const dispatch = useDispatch();
    const jogs = useSelector(getJogs);
    const dateFrom = useSelector(getDateFrom);
    const dateTo = useSelector(getDateTo);
    const IsDateSelected = dateFrom || dateTo;

    const filteredItems = (): IJog[] => {
        let filteringJogs = jogs;

        if (dateFrom) filteringJogs = jogs.filter(jog => jog.date > dateFrom);
        if (dateTo) filteringJogs = jogs.filter(jog => jog.date < dateTo);

        return filteringJogs;
    };

    useEffect(() => {
        dispatch(loadJogs());
    }, [dispatch]);

    const jogsForShow = IsDateSelected ? filteredItems() : jogs;

    return (
        <SJogWrapper>
            {jogsForShow.length ? (
                jogsForShow.map((jog: IJog) => <Jog key={jog.id} distance={jog.distance} time={jog.time} date={jog.date} speed={jog.speed} />)
            ) : (
                <NothingToShow />
            )}
        </SJogWrapper>
    );
};

export default JogsList;
