import styled from "styled-components";
import { Link } from "react-router-dom";

import { white, green } from "../../../styles/colors";

export const headerHeightDesktop = 116;

export const SHeaderWrapper = styled.div`
    position: fixed;
    left: 0;
    top: 0;

    width: 100%;

    display: flex;
    align-content: center;
    justify-content: space-between;
    flex-wrap: wrap;
`;

export const SHeader = styled.div`
    width: 100%;
    height: ${headerHeightDesktop}px;
    padding: 30px 30px 30px 37px;
    background-color: ${green};
    z-index: 1;

    flex: 1 1 100%;

    display: flex;
    align-content: center;
    justify-content: space-between;
`;

export const SNavWrapper = styled.div`
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;

    svg {
        cursor: pointer;
    }
`;

export const SLink = styled(Link)<{ isActive: boolean }>`
    margin: 20px 20px 14px;
    font-family: "Roboto", sans-serif;
    font-size: 14px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    text-decoration: none;
    text-transform: uppercase;
    color: ${white};
    ${({ isActive }) => isActive && `border-bottom: solid 2px ${white};`}
`;

export const SChildrenWrapper = styled.div<{ isActive: boolean }>`
    width: 100%;
    transform: ${({ isActive }) => (isActive ? "translateY(0)" : "translateY(-100%)")};
    transition: transform 0.3s ease-in-out;
    z-index: 0;
`;
