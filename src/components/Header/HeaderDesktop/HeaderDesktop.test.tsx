import React from "react";
import { BrowserRouter } from "react-router-dom";
import renderer from "react-test-renderer";
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure  } from "enzyme";
import HeaderDesktop from "./";
import { SLink } from "./style";

configure({adapter: new Adapter()});
describe("HeaderDesktop", () => {
  it("should render correctly", () => {
    const tree = renderer
      .create(
        <BrowserRouter>
            <HeaderDesktop showFilterBtn showMenu/>
        </BrowserRouter>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("should render without menu", () => {
    const tree = renderer
      .create(
        <BrowserRouter>
            <HeaderDesktop showFilterBtn showMenu={false} />
        </BrowserRouter>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("should render without filter", () => {
    const tree = renderer
      .create(
        <BrowserRouter>
            <HeaderDesktop showFilterBtn={false} showMenu/>
        </BrowserRouter>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
