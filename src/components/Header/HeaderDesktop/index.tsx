import React, { FunctionComponent, useState } from "react";
import { useLocation } from "react-router-dom";

import { ReactComponent as Logo } from "../../../static/logo.svg";
import { ReactComponent as Filter } from "../../../static/filter.svg";
import { ReactComponent as FilterActive } from "../../../static/filter-active.svg";
import { IHeaderProps, navs } from "../index";

import { SHeader, SNavWrapper, SLink, SHeaderWrapper, SChildrenWrapper } from "./style";

export const HeaderDesktop: FunctionComponent<IHeaderProps> = ({ showMenu, showFilterBtn, children }) => {
    const [showChildren, setShowChildren] = useState(false);
    const { pathname } = useLocation();

    const toggleShowChildren = () => setShowChildren(!showChildren);

    return (
        <SHeaderWrapper>
            <SHeader>
                <Logo />
                <SNavWrapper>
                    {showMenu &&
                        navs.map(nav => (
                            <SLink to={nav.href} isActive={pathname === nav.href} key={nav.label}>
                                {nav.label}
                            </SLink>
                        ))}
                    {showFilterBtn && (showChildren ? <FilterActive onClick={toggleShowChildren} /> : <Filter onClick={toggleShowChildren} />)}
                </SNavWrapper>
            </SHeader>
            <SChildrenWrapper isActive={showChildren}>{children}</SChildrenWrapper>
        </SHeaderWrapper>
    );
};

export default HeaderDesktop;
