import React, { FunctionComponent, ReactChild } from "react";
import MediaQuery from "react-responsive";

import HeaderDesktop from "./HeaderDesktop";
import HeaderMobile from "./HeaderMobile";

export interface INav {
    label: string;
    href: string;
}

export const navs: INav[] = [
    {
        label: "Jogs",
        href: "/jogs"
    },
    {
        label: "Info",
        href: "/info"
    },
    {
        label: "Contact us",
        href: "/contact"
    }
];

export interface IHeaderProps {
    showMenu: boolean;
    showFilterBtn: boolean;
    children?: ReactChild | ReactChild[];
}

export const Header: FunctionComponent<IHeaderProps> = props => {
    return (
        <>
            <MediaQuery minWidth={769}>
                <HeaderDesktop {...props} />
            </MediaQuery>
            <MediaQuery maxWidth={768}>
                <HeaderMobile {...props} />
            </MediaQuery>
        </>
    );
};

export default Header;
