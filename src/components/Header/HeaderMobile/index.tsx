import React, { FunctionComponent, useState } from "react";
import { useLocation } from "react-router-dom";

import { IHeaderProps, navs } from "../index";

import {
    SHeaderMobile,
    SNavWrapperMobile,
    SHeaderWrapperMobile,
    SChildrenWrapperMobile,
    SFilter,
    SFilterActive,
    SLogo,
    SMenu,
    SNav,
    SClose,
    SLogoGreen,
    SLinkMobile,
    SLinkWrapper
} from "./style";

export const HeaderMobile: FunctionComponent<IHeaderProps> = ({ showMenu, showFilterBtn, children }) => {
    const [showChildren, setShowChildren] = useState(false);
    const [showSidebar, setShowSidebar] = useState(false);
    const { pathname } = useLocation();

    const toggleShowChildren = () => setShowChildren(!showChildren);
    const toggleShowSidebar = () => setShowSidebar(!showSidebar);

    return (
        <SHeaderWrapperMobile>
            <SHeaderMobile>
                <SLogo />
                <SNavWrapperMobile>
                    {showFilterBtn && (showChildren ? <SFilterActive onClick={toggleShowChildren} /> : <SFilter onClick={toggleShowChildren} />)}
                    {showMenu && <SMenu onClick={toggleShowSidebar} />}
                </SNavWrapperMobile>
            </SHeaderMobile>
            <SChildrenWrapperMobile isActive={showChildren}>{children}</SChildrenWrapperMobile>
            <SNav isActive={showSidebar}>
                <SLogoGreen />
                <SClose onClick={toggleShowSidebar} />
                <SLinkWrapper>
                    {navs.map(nav => (
                        <SLinkMobile to={nav.href} isActive={pathname === nav.href} key={nav.label}>
                            {nav.label}
                        </SLinkMobile>
                    ))}
                </SLinkWrapper>
            </SNav>
        </SHeaderWrapperMobile>
    );
};

export default HeaderMobile;
