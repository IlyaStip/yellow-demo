import React from "react";
import { BrowserRouter } from "react-router-dom";
import renderer from "react-test-renderer";
import Adapter from 'enzyme-adapter-react-16';
import { configure  } from "enzyme";
import HeaderMobile from ".";

configure({adapter: new Adapter()});
describe("HeaderMobile", () => {
  it("should render correctly", () => {
    const tree = renderer
      .create(
        <BrowserRouter>
            <HeaderMobile showFilterBtn showMenu/>
        </BrowserRouter>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
