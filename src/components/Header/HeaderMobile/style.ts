import styled from "styled-components";

import { ReactComponent as Logo } from "../../../static/logo.svg";
import { ReactComponent as Filter } from "../../../static/filter.svg";
import { ReactComponent as FilterActive } from "../../../static/filter-active.svg";
import { ReactComponent as Menu } from "../../../static/menu.svg";
import { ReactComponent as Cancel } from "../../../static/cancel.svg";
import { white, green, darkGray } from "../../../styles/colors";
import { SChildrenWrapper, SHeader, SHeaderWrapper, SLink, SNavWrapper } from "../HeaderDesktop/style";

export const headerHeightMobile = 77;

export const SHeaderWrapperMobile = styled(SHeaderWrapper)`
    z-index: 1;
`;

export const SHeaderMobile = styled(SHeader)`
    height: ${headerHeightMobile}px;
    padding: 30px 30px 30px 37px;
`;

export const SLogo = styled(Logo)`
    height: 39px;
    width: auto;
`;

export const SLogoGreen = styled(SLogo)`
    position: absolute;
    left: 25px;
    top: 20px;

    path {
        fill: ${green};
    }
`;

export const SFilterActive = styled(FilterActive)`
    width: 39px;
    height: 39px;
`;

export const SFilter = styled(Filter)`
    width: 39px;
    height: 26px;
`;

export const SMenu = styled(Menu)`
    width: 39px;
    height: 39px;
    margin-left: 45px;
`;

export const SClose = styled(Cancel)`
    width: 26px;
    height: 26px;

    position: absolute;
    right: 25px;
    top: 20px;

    path {
        fill: ${darkGray};
    }
`;

export const SChildrenWrapperMobile = styled(SChildrenWrapper)`
    width: 100%;
    transform: ${({ isActive }) => (isActive ? "translateY(0)" : "translateY(-100%)")};
    transition: transform 0.3s ease-in-out;
    z-index: 0;
`;

export const SNavWrapperMobile = styled(SNavWrapper)``;

export const SNav = styled.div<{ isActive: boolean }>`
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: ${white};
    z-index: 2;
    padding: 20px 25px;

    transform: ${({ isActive }) => (isActive ? "translateY(0)" : "translateX(100%)")};
    transition: transform 0.3s ease-in-out;

    display: flex;
    justify-content: center;
    align-items: center;
`;

export const SLinkWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
`;

export const SLinkMobile = styled(SLink)`
    flex: 1 1 100%;
`;
