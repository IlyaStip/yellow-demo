import React, { FunctionComponent } from "react";
import { useDispatch, useSelector } from "react-redux";
import MediaQuery from "react-responsive";

import { getDateFrom, getDateTo, setDateFrom, setDateTo } from "../../app/reducerSlices/jogSlice";

import { SDataFilter, SDatePickerWrapper, SDatePicker, SDatePickerLabel, SDatePickerText } from "./style";

export const dataFormats = ["dd.MM.yyyy", "dd/MM/yyyy"];

export const DataFilter: FunctionComponent = () => {
    const dispatch = useDispatch();
    const startDate = useSelector(getDateFrom);
    const endDate = useSelector(getDateTo);

    return (
        <MediaQuery maxWidth={769}>
            {matches => (
                <SDataFilter>
                    <SDatePickerWrapper>
                        <SDatePickerLabel>
                            <SDatePickerText>Date from</SDatePickerText>
                            <SDatePicker
                                dateFormat={dataFormats}
                                selectsStart
                                selected={startDate}
                                endDate={endDate}
                                withPortal={matches}
                                onChange={date => {
                                    if (Array.isArray(date)) return;
                                    dispatch(setDateFrom(date));
                                }}
                            />
                        </SDatePickerLabel>
                        <SDatePickerLabel>
                            <SDatePickerText>Date to</SDatePickerText>
                            <SDatePicker
                                dateFormat={dataFormats}
                                selectsEnd
                                selected={endDate}
                                minDate={startDate}
                                withPortal={matches}
                                onChange={date => {
                                    if (Array.isArray(date)) return;
                                    dispatch(setDateTo(date));
                                }}
                            />
                        </SDatePickerLabel>
                    </SDatePickerWrapper>
                </SDataFilter>
            )}
        </MediaQuery>
    );
};

export default DataFilter;
