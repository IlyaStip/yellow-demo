import styled from "styled-components";
import DatePicker from "react-datepicker";

import { gray, darkGray, white } from "../../styles/colors";

export const SDataFilter = styled.div`
    height: 60px;

    background-color: ${gray};

    flex: 1 1 100%;

    display: flex;
    align-content: center;
    justify-content: center;
    align-items: center;
`;

export const SDatePickerWrapper = styled.div`
    flex: 0 1 375px;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const SDatePickerLabel = styled.label`
    display: flex;
    flex-wrap: nowrap;
    align-content: center;
    justify-content: space-between;
`;

export const SDatePickerText = styled.p`
    flex: 0 1 50%;
    display: flex;
    align-items: center;
    font-family: "Roboto", sans-serif;
    font-size: 13px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: ${darkGray};
`;

export const SDatePicker = styled(DatePicker)`
    width: 100px;
    height: 31px;
    margin: 0 15px 0 15px;
    border-radius: 11px;
    border: solid 1px ${darkGray};
    padding: 10px 20px;
    background-color: ${white};

    .react-datepicker-wrapper {
        height: 31px;
    }
`;
