import React, { FunctionComponent } from "react";
import { useDispatch } from "react-redux";
import MediaQuery from "react-responsive";

import { ReactComponent as BearFace } from "../../static/bear-face.svg";
import { createAccessToken } from "../../app/reducerSlices/userSlice";
import { Button, ButtonTypes } from "../Button";

import { SAlert, SAlertImg } from "./style";

export const LoginAlert: FunctionComponent = () => {
    const dispatch = useDispatch();

    const handleClick = () => dispatch(createAccessToken());

    return (
        <SAlert>
            <MediaQuery minWidth={769}>
                <BearFace />
            </MediaQuery>
            <MediaQuery maxWidth={768}>
                <SAlertImg src="bearFace.png" alt="Bear face" />
            </MediaQuery>
            <MediaQuery minWidth={769}>{matches => <Button text="Let me in" styleType={matches ? ButtonTypes.White : ButtonTypes.Blue} onClick={handleClick} />}</MediaQuery>
        </SAlert>
    );
};

export default LoginAlert;
