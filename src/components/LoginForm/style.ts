import styled from "styled-components";

import { blue, white } from "../../styles/colors";

export const SAlert = styled.div`
    flex: 0 1 250px;
    height: 100%;
    max-width: 503px;
    max-height: 379px;
    border-radius: 44px;
    background-color: ${blue};

    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    padding: 77px 171px 36px;

    svg {
        margin-bottom: 55px;
    }

    @media (max-width: 768px) {
        padding: 0;
        background-color: ${white};
        max-height: 100%;

        svg {
            path {
                fill: ${blue};
            }
        }
    }
`;

export const SAlertImg = styled.img``;
