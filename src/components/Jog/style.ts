import styled from "styled-components";

import { black, darkGray, gray } from "../../styles/colors";

export const SJogWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-content: center;

    padding: 45px 0;

    @media (max-width: 768px) {
        border-bottom: solid 1px ${gray};
    }
`;

export const SJog = styled.div`
    max-width: 340px;
    width: 100%;
    height: 100px;

    display: flex;
    align-content: center;
    justify-content: space-between;

    svg {
        flex: 1 1 100%;
    }
`;

export const SInfoWrapper = styled.div`
    flex: 1 1 100%;

    display: flex;
    align-content: center;
    justify-content: flex-start;
    flex-wrap: wrap;
    margin-left: 45px;
`;

export const SRow = styled.p`
    flex: 1 1 100%;
    margin-bottom: 12px;
`;

export const STitle = styled.span`
    font-family: "Roboto", sans-serif;
    font-size: 14px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: ${black};
`;

export const SValue = styled.span`
    font-family: "Roboto", sans-serif;
    font-size: 14px;
    font-weight: 400;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: ${darkGray};
`;
