import React, { FunctionComponent } from "react";

import { ReactComponent as JogIcon } from "../../static/jogIcon.svg";
import convertDateToFormat from "../../utils/convertDateToFormat";

import { SJog, SInfoWrapper, SRow, SValue, STitle, SJogWrapper } from "./style";

export interface IJogProps {
    distance: number;
    time: number;
    date: Date;
    speed: number;
}

const Jog: FunctionComponent<IJogProps> = ({ distance, time, date, speed }) => (
    <SJogWrapper>
        <SJog>
            <JogIcon />
            <SInfoWrapper>
                <SRow>
                    <SValue>{convertDateToFormat(date)}</SValue>
                </SRow>
                <SRow>
                    <STitle>Speed: </STitle>
                    <SValue>{speed}</SValue>
                </SRow>
                <SRow>
                    <STitle>Distance: </STitle>
                    <SValue>{`${distance} km`}</SValue>
                </SRow>
                <SRow>
                    <STitle>Time: </STitle>
                    <SValue>{`${time} min`}</SValue>
                </SRow>
            </SInfoWrapper>
        </SJog>
    </SJogWrapper>
);

export default Jog;
