import React, { FunctionComponent } from "react";

import SButton from "./style";

export enum ButtonTypes {
    White,
    Blue
}

export interface IButton {
    text: string;
    styleType: ButtonTypes;
    onClick?: () => void;
    buttonType?: "button" | "submit";
    fillSpace?: boolean;
}

export const Button: FunctionComponent<IButton> = ({ text, styleType, onClick, buttonType, fillSpace = false }) => (
    <SButton styleType={styleType} onClick={onClick} type={buttonType} fillSpace={fillSpace}>
        {text}
    </SButton>
);

export default Button;
