import styled from "styled-components";

import { blue, white } from "../../styles/colors";

import { ButtonTypes } from ".";

const SButton = styled.button<{ styleType: ButtonTypes; fillSpace: boolean }>`
    flex: ${({ fillSpace }) => (fillSpace ? `1 1 100%` : "0 1 250px")};
    height: 60px;
    margin: 0px;
    border-radius: 36px;
    border: ${({ styleType }) => {
        if (styleType === ButtonTypes.White) return `solid 3px ${white}`;
        if (styleType === ButtonTypes.Blue) return `solid 3px ${blue}`;

        return `solid 3px ${blue}`;
    }};
    color: ${({ styleType }) => {
        if (styleType === ButtonTypes.White) return `${white}`;
        if (styleType === ButtonTypes.Blue) return `${blue}`;

        return `${blue}`;
    }};
    background-color: transparent;
    padding: 0px 25px;

    font-family: "Roboto", sans-serif;
    font-size: 18px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;

    cursor: pointer;
`;

export default SButton;
