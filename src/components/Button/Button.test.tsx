import React from "react";
import renderer from "react-test-renderer";
import Adapter from 'enzyme-adapter-react-16';
import { configure  } from "enzyme";
import 'jest-styled-components';
import { Button, ButtonTypes } from ".";
import { blue, white } from "../../styles/colors";

configure({adapter: new Adapter()});
describe("Button", () => {
  it("should render correctly", () => {
    const tree = renderer
      .create(
        <Button text="Test text" styleType={ButtonTypes.Blue} />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("should be Blue", () => {
    const tree = renderer
      .create(
        <Button text="Test text" styleType={ButtonTypes.Blue} />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
    expect(tree).toHaveStyleRule('color', blue)
    expect(tree).toHaveStyleRule('border', `solid 3px ${blue}`)
  });

  it("should be White", () => {
    const tree = renderer
      .create(
        <Button text="Test text" styleType={ButtonTypes.White} />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
    expect(tree).toHaveStyleRule('color', white)
    expect(tree).toHaveStyleRule('border', `solid 3px ${white}`)
  });

  it("should be filled space", () => {
    const tree = renderer
      .create(
        <Button text="Test text" styleType={ButtonTypes.White} fillSpace />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
    expect(tree).toHaveStyleRule('flex', `1 1 100%`)
  });
});
