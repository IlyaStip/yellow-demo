import React, { FunctionComponent, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Switch, Route, Redirect, useLocation } from "react-router";

import { checkIsAuthorized, getIsAuth } from "../app/reducerSlices/userSlice";
import ContactPage from "../components/Pages/ContactPage";
import JogsPage from "../components/Pages/JogsPage";
import LoginPage from "../components/Pages/LoginPage";
import AddJogPage from "../components/Pages/AddJogPage";

const RootRouter: FunctionComponent = () => {
    const { pathname } = useLocation();
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(checkIsAuthorized());
    }, [pathname, dispatch]);

    return (
        <Switch>
            {useSelector(getIsAuth) ? (
                <>
                    <Route path="/add" exact component={AddJogPage} />
                    <Route path="/jogs" exact component={JogsPage} />
                    <Route path="/info" exact component={ContactPage} />
                    <Route path="/contact" exact component={ContactPage} />
                    <Redirect to="/jogs" />
                </>
            ) : (
                <>
                    <Route path="/" exact component={LoginPage} />
                    <Redirect to="/" />
                </>
            )}
        </Switch>
    );
};

export default RootRouter;
